#include <stdbool.h>

#include "ai.h"

#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#define MAX_MENU_ITEM_LENGTH 20

typedef enum
{
	ControlUp,
	ControlDown,
	ControlLeft,
	ControlRight,
	ControlSelect
}
Control;

typedef enum
{
	GameStateSplash,
	GameStateMenuMain,
	GameStateMenuStart,
	GameStateGameSelectX,
	GameStateGameSelectO,
	GameStateGameWinX,
	GameStateGameWinO,
	GameStateGameTie
}
GameState;

typedef enum
{
	GameValueEmpty,
	GameValueX,
	GameValueO
}
GameValue;

typedef enum
{
	GameType1vs1,
	GameType1vsCPU
}
GameType;

typedef enum
{
	GameStarterX,
	GameStarterO,
	GameStarterPlayer,
	GameStarterCPU
}
GameStarter;

static const int GameWinningMoves[8][3] =
{
	{ 0, 3, 6 },
	{ 1, 4, 7 },
	{ 2, 5, 8 },
	{ 0, 1, 2 },
	{ 3, 4, 5 },
	{ 6, 7, 8 },
	{ 0, 4, 8 },
	{ 2, 4, 6 }
};

void GameInitialize();
void GameShowMenuMain();
void GameShowMenuStart(GameType type);
void GameStartGame(GameStarter starter);
void GameControlCallback(Control control);
bool GameDoMove(int index);

unsigned int GameGetSelected();
void GameSetSelected(int index);
GameState GameGetState();
GameValue* GameGetField();
GameType GameGetGameType();
unsigned char GameGetScoreWinX();
unsigned char GameGetScoreWinO();
unsigned char GameGetScoreTies();

#endif