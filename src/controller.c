#include <hw_memmap.h>
#include <gpio.h>
#include <sysctl.h>

#include "main.h"
#include "game.h"
#include "controller.h"

void ControllerInitPort(unsigned long port, unsigned char pins);

void ControllerInitialize()
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

	ControllerInitPort(BUS_DPAD, SWITCHES_DPAD);
	ControllerInitPort(BUS_CONTROL, SWITCHES_CONTROL);
}

void ControllerInitPort(unsigned long port, unsigned char pins)
{
	GPIOPinTypeGPIOInput(port, pins);
	GPIOPadConfigSet(port, pins, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
}

void ControllerPoll()
{
	static unsigned long lastPoll;
	static unsigned long lastPress;

	if(lastPoll + POLLING_INTERVAL > GetSystemCounter())
	{
		return;
	}
	
	lastPoll = GetSystemCounter();
	
	char dpadPins = (char) (~GPIOPinRead(BUS_DPAD, SWITCHES_DPAD) & 0x0F);
	char controlPins = (char) (~GPIOPinRead(BUS_CONTROL, SWITCHES_CONTROL) & 0x02);
	
	if(dpadPins > 0)
	{
		if(lastPress + PRESS_INTERVAL > GetSystemCounter())
		{
			return;
		}
		lastPress = GetSystemCounter();
		
		switch(dpadPins)
		{
			case SWITCH_UP:		GameControlCallback(ControlUp); break;
			case SWITCH_DOWN:	GameControlCallback(ControlDown); break;
			case SWITCH_LEFT:	GameControlCallback(ControlLeft); break;
			case SWITCH_RIGHT:	GameControlCallback(ControlRight); break;
			default: break;
		}
	}

	if(controlPins == SWITCH_SELECT)
	{
		if(lastPress + PRESS_INTERVAL > GetSystemCounter())
		{
			return;
		}
		lastPress = GetSystemCounter();

		GameControlCallback(ControlSelect);
	}
}