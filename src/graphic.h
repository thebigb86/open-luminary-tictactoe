#ifndef GRAPHIC_H_INCLUDED
#define GRAPHIC_H_INCLUDED

typedef struct
{
	const unsigned char* bitmap;
	unsigned long x;
	unsigned long y;
	unsigned long width;
	unsigned long height;
} 
Graphic;

typedef enum
{
	GraphicSplash,
	GraphicGrid,
	GraphicFieldEmpty,
	GraphicFieldEmptySelected,
	GraphicFieldX,
	GraphicFieldXSelected,
	GraphicFieldO,
	GraphicFieldOSelected,
	GraphicWinX,
	GraphicWinO,
	GraphicTie
}
AvailableGraphics;

void GraphicSetPosition(Graphic *graphic, unsigned long x, unsigned long y);
void GraphicSetSize(Graphic *graphic, unsigned long width, unsigned long height);
const unsigned char* GraphicGet(AvailableGraphics graphic);

#endif