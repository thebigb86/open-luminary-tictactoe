#include "game.h"
#include "graphic.h"

#ifndef VIEW_H_INCLUDED
#define VIEW_H_INCLUDED

void ViewInitialize();
void ViewClear();
void ViewDraw(Graphic* graphic);
void ViewDrawScore();

void ViewDrawMenu(const char menuItems[][MAX_MENU_ITEM_LENGTH], char countMenuItems);
void ViewDrawMenuSelectedItem(char selectedItem, char countItems);

void ViewDrawSplash();
void ViewDrawGrid();
void ViewDrawField(GameValue value, char position, bool isSelected);
void ViewDrawFinish();

#endif