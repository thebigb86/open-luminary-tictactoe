#include <sysctl.h>
#include <systick.h>

#include "main.h"
#include "game.h"
#include "view.h"
#include "controller.h"

unsigned long systemClock;

unsigned long systemCounter;

unsigned long sleepCounter;

bool tickFlag;

void IntSysTick();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
int main()
{
	SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN | SYSCTL_XTAL_8MHZ);
	SysCtlPWMClockSet(SYSCTL_PWMDIV_8);
	
	ViewInitialize();
	GameInitialize();
	ControllerInitialize();

	systemClock = SysCtlClockGet();
	SysTickPeriodSet(systemClock / 1000);
	SysTickIntRegister(IntSysTick);
	SysTickIntEnable();
	SysTickEnable();

	while(1) 
	{
		if(tickFlag)
		{
			if(sleepCounter > 0)
			{
				sleepCounter--;
				tickFlag = false;
			}
			else
			{
				ControllerPoll();
				tickFlag = false;
			}
		}
	}
}
#pragma clang diagnostic pop

unsigned long GetSystemCounter()
{
	return systemCounter;
}

void SetSleepCounter(unsigned long period)
{
	sleepCounter = period;
}

void IntSysTick()
{
	systemCounter++;
	tickFlag = true;
}