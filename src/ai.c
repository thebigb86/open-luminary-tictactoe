#include "ai.h"
#include "game.h"

const int AiPreferredMoves[9] = { 4, 0, 2, 6, 8, 1, 3, 5, 7 };

void AiDoMove()
{
	for (int entry = 0; entry < 8; entry++)
	{
		int winConditionCounter = 0;
		char winConditionAdvantage = ' ';

		for (int i = 0; i < 3; i++)
		{
			GameValue value = GameGetField()[GameWinningMoves[entry][i]];
			if (value != GameValueEmpty) 
			{
				if (value == GameValueX && winConditionAdvantage != 'O')
				{
					winConditionAdvantage = 'X';
					winConditionCounter++;
				}
				else if (value == GameValueO && winConditionAdvantage != 'X')
				{
					winConditionAdvantage = 'O';
					winConditionCounter++;
				}
				else 
				{ 
					break;
				}
			}
		}

		if (winConditionCounter == 2)
		{
			for (int i = 0; i < 3; i++) 
			{
				GameValue value = GameGetField()[GameWinningMoves[entry][i]];
				int field = GameWinningMoves[entry][i];
				if (value == GameValueEmpty) 
				{
					GameDoMove(field);
					return;
				}
			}
		}
	}

	for (int i = 0; i < 9; i++) 
	{
		GameValue currentField = GameGetField()[AiPreferredMoves[i]];
		if (currentField == GameValueEmpty) 
		{
			GameDoMove(AiPreferredMoves[i]);
			return;
		}
	}
}