#include <stdbool.h>

#include "main.h"
#include "game.h"
#include "view.h"

#define NUM_MENU_MAIN_ITEMS 3
#define NUM_MENU_START_1VS1_ITEMS 2
#define NUM_MENU_START_PLAYERVSCPU_ITEMS 2

const char MenuMain[][MAX_MENU_ITEM_LENGTH] = {
	"  1 vs 1",
	"  Player vs CPU",
	"  Reset score"
};

const char MenuStart1vs1[][MAX_MENU_ITEM_LENGTH] = {
	"  X starts",
	"  O starts"
};

const char MenuStartPlayervsCPU[][MAX_MENU_ITEM_LENGTH] = {
	"  CPU starts (X)",
	"  Player starts (O)"
};

GameValue grid[9];
GameState state;
GameType type;
GameStarter starter;

unsigned char selected;
unsigned char menuSelected;
unsigned char menuItemCount;
unsigned char scoreWinX;
unsigned char scoreWinO;
unsigned char scoreTies;

void GameResetField();
bool GameIsWin();
bool GameIsTie();
void GameRedrawFields();
bool GameCheckForWin();
bool GameCheckForTie();
void GameResetScore();

///
/// Public methods
///


void GameInitialize()
{
	GameResetScore();
	state = GameStateSplash;
	ViewDrawSplash();
	SetSleepCounter(500);
}

void GameShowMenuMain()
{
	state = GameStateMenuMain;

	menuSelected = 0;
	menuItemCount = NUM_MENU_MAIN_ITEMS;

	ViewDrawMenu(MenuMain, menuItemCount);
	ViewDrawMenuSelectedItem(menuSelected, menuItemCount);
	ViewDrawScore();

	SetSleepCounter(500);
}

void GameShowMenuStart(GameType gameType)
{
	type = gameType;
	state = GameStateMenuStart;
	menuSelected = 0;

	if(type == GameType1vs1)
	{
		menuItemCount = NUM_MENU_START_1VS1_ITEMS;
		ViewDrawMenu(MenuStart1vs1, menuItemCount);
		ViewDrawMenuSelectedItem(menuSelected, menuItemCount);
	}
	else
	{
		menuItemCount = NUM_MENU_START_PLAYERVSCPU_ITEMS;
		ViewDrawMenu(MenuStartPlayervsCPU, menuItemCount);
		ViewDrawMenuSelectedItem(menuSelected, menuItemCount);
	}
	
	ViewDrawScore();
	
	SetSleepCounter(500);
}

void GameStartGame(GameStarter starter)
{
	GameResetField();
	selected = 0;

	switch(starter)
	{
		case GameStarterX:
		case GameStarterCPU:

			state = GameStateGameSelectX;
			break;
			
		case GameStarterO:
		case GameStarterPlayer:

			state = GameStateGameSelectO;
			break;
	}
	
	ViewClear();
	ViewDrawGrid();
	ViewDrawField(GameValueEmpty, 0, true);
	ViewDrawScore();

	if(starter == GameStarterCPU)
	{
		AiDoMove();
	}

	SetSleepCounter(500);
}

void GameControlCallback(Control control)
{
	switch(state)
	{
		case GameStateSplash:

			GameShowMenuMain();
			return;

		case GameStateMenuMain:
		case GameStateMenuStart:

			if(control == ControlUp || control == ControlDown)
			{
				menuSelected = (unsigned char)
					((control == ControlUp) ? menuSelected - 1 : menuSelected + 1) % 
					menuItemCount
				;
				ViewDrawMenuSelectedItem(menuSelected, menuItemCount);
			}

			if(control == ControlSelect)
			{
				if(state == GameStateMenuMain)
				{
					switch(menuSelected)
					{
						case 0: GameShowMenuStart(GameType1vs1); break;
						case 1: GameShowMenuStart(GameType1vsCPU); break;
						case 2: 
							GameResetScore();
							ViewDrawScore();
							break;

						default:
							break;
					}
				}
				else if(state == GameStateMenuStart)
				{
					if(type == GameType1vs1)
					{
						GameStartGame((menuSelected == 0) ? GameStarterX : GameStarterO);
					}
					else
					{
						GameStartGame((menuSelected == 0) ? GameStarterCPU : GameStarterPlayer);
					}
				}
			}
			return;

		case GameStateGameSelectX:
		case GameStateGameSelectO:
			
			if(type == GameType1vsCPU && state == GameStateGameSelectX)
			{
				break;
			}

			switch(control)
			{
				case ControlUp:		GameSetSelected(selected - 3); return;
				case ControlDown:	GameSetSelected(selected + 3); return;
				case ControlLeft:	GameSetSelected(selected - 1); return;
				case ControlRight:	GameSetSelected(selected + 1); return;
				case ControlSelect:	GameDoMove(selected);
			}
			break;

		case GameStateGameWinX:
		case GameStateGameWinO:
		case GameStateGameTie:

			if(control == ControlSelect)
			{
				GameShowMenuMain();
			}
			return;
	}
}

unsigned int GameGetSelected()
{
	return selected;
}

void GameSetSelected(int index)
{
	if (index >= 0 && index < 9)
	{
		selected = index;
		GameRedrawFields();
	}
}

GameType GameGetGameType()
{
	return type;
}

GameState GameGetState()
{
	return state;
}

GameValue* GameGetField()
{
	return grid;
}

unsigned char GameGetScoreWinX()
{
	return scoreWinX;
}

unsigned char GameGetScoreWinO()
{
	return scoreWinO;
}

unsigned char GameGetScoreTies()
{
	return scoreTies;
}

bool GameDoMove(int index)
{
	// Are we in-game?
	if (state != GameStateGameSelectX && state != GameStateGameSelectO)
	{
		return false;
	}
	// Index within bounds, and the value at the given index empty?
	if (index >= 0 && index < 9 && grid[index] == GameValueEmpty)
	{
		grid[index] = (state == GameStateGameSelectX) ? GameValueX : GameValueO;

		// Check whether this makes a win
		if (GameCheckForWin())
		{
			return true;
		}

		if (GameCheckForTie())
		{
			return true;
		}
		
		if (type == GameType1vsCPU && state == GameStateGameSelectX)
		{
			AiDoMove();

			if(state != GameStateGameSelectO && state != GameStateGameSelectX)
			{
				return true;
			}
		}
			
		GameRedrawFields();
		return true;
	}

	// Implicit else
	return false;
}

///
/// Private methods
///

void GameResetField()
{
	for (int i = 0; i < 9; i++)
	{
		grid[i] = GameValueEmpty;
	}
}

bool GameIsWin()
{
	for (int entry = 0; entry < 8; entry++)
	{
		bool winX = true;
		bool winO = true;

		for(int i = 0; i < 3; i++)
		{
			GameValue value = grid[GameWinningMoves[entry][i]];

			if(winX && value != GameValueX)
			{
				winX = false;
			}
			
			if(winO && value != GameValueO)
			{
				winO = false;
			}
		}

		if(winX)
		{
			scoreWinX++;
			scoreWinX = scoreWinX % 99; // Prevent buffer overflow in score string
			state = GameStateGameWinX;
			return true;
		}

		if(winO)
		{
			scoreWinO++;
			scoreWinO = scoreWinO % 99; // Prevent buffer overflow in score string
			state = GameStateGameWinO;
			return true;
		}
	}

	return false;
}

bool GameIsTie()
{
	for(int i = 0; i < 9; i++)
	{
		if(grid[i] == GameValueEmpty)
		{
			return false;
		}
	}

	scoreTies++;
	scoreTies = scoreTies % 99; // Prevent buffer overflow in score string
	state = GameStateGameTie;
	return true;
}

void GameRedrawFields()
{
	for(int i = 0; i < 9; i++)
	{
		ViewDrawField(grid[i], i, i == selected);
	}
}

bool GameCheckForWin()
{
	if (!GameIsWin())
	{
		// Switch game state to other player
		state = (state == GameStateGameSelectX) ?
			GameStateGameSelectO :
			GameStateGameSelectX
		;
		return false;
	}

	ViewClear();
	ViewDrawFinish();
	SetSleepCounter(500);

	return true;
}

bool GameCheckForTie()
{
	if (GameIsTie())
	{
		ViewClear();
		ViewDrawFinish();
		SetSleepCounter(500);
		return true;
	}

	return false;
}

void GameResetScore()
{
	scoreWinX = 0;
	scoreWinO = 0;
	scoreTies = 0;
}