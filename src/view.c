#include <stdio.h>
#include <math.h>

#include "view.h"
#include "rit128x96x4.h"

void ViewInitialize()
{
	// Initialize display
	RIT128x96x4Init(1000000);
}

void ViewClear()
{
	RIT128x96x4Clear();
}

void ViewDraw(Graphic* graphic)
{
	RIT128x96x4ImageDraw(graphic->bitmap, graphic->x, graphic->y, graphic->width, graphic->height);
}

void ViewDrawScore()
{
	char scoreBuffer[22];
	sprintf(scoreBuffer, "X:%u  O:%u  Ties:%u", GameGetScoreWinX(), GameGetScoreWinO(), GameGetScoreTies());

	RIT128x96x4StringDraw("                      ", 0, 0, 12);
	RIT128x96x4StringDraw(scoreBuffer, 0, 0, 12);
}

void ViewDrawMenu(const char menuItems[][MAX_MENU_ITEM_LENGTH], char countMenuItems)
{
	RIT128x96x4Clear();
	for(int i = 0; i < countMenuItems; i++)
	{
		RIT128x96x4StringDraw(menuItems[i], 8, (unsigned long) (32 + i * 8), 12);
	}
}

void ViewDrawMenuSelectedItem(char selectedItem, char countItems)
{
	for(int i = 0; i < countItems; i++)
	{
		RIT128x96x4StringDraw("*", 8, (unsigned long) (32 + i * 8), (unsigned char) ((i == selectedItem) ? 12 : 0));
	}
}

void ViewDrawSplash()
{
	Graphic splash;
	splash.bitmap = GraphicGet(GraphicSplash);
	GraphicSetPosition(&splash, 0, 0);
	GraphicSetSize(&splash, 128, 96);
	ViewDraw(&splash);
}

void ViewDrawGrid()
{
	Graphic grid;
	grid.bitmap = GraphicGet(GraphicGrid);
	
	GraphicSetPosition(&grid, 30, 24);
	GraphicSetSize(&grid, 68, 68);
	ViewDraw(&grid);
}

void ViewDrawField(GameValue value, char position, bool isSelected)
{
	Graphic field;
	GraphicSetSize(&field, 20, 20);

	switch(value)
	{
		case GameValueEmpty:
			field.bitmap = GraphicGet(!isSelected ? GraphicFieldEmpty : GraphicFieldEmptySelected);
			break;
	
		case GameValueX:
			field.bitmap = GraphicGet(!isSelected ? GraphicFieldX : GraphicFieldXSelected);
			break;

		case GameValueO:
			field.bitmap = GraphicGet(!isSelected ? GraphicFieldO : GraphicFieldOSelected);
			break;
	}

	int fieldX = position % 3;
	int fieldY = (int)floor(position / 3.0);
	int screenX = 32 + (22 * fieldX);
	int screenY = 26 + (22 * fieldY);

	GraphicSetPosition(&field, (unsigned long)screenX, (unsigned long)screenY);

	ViewDraw(&field);
}

void ViewDrawFinish()
{
	Graphic finish;
	GraphicSetSize(&finish, 110, 40);
	GraphicSetPosition(&finish, 9, 28);

	switch(GameGetState())
	{
		case GameStateGameWinX:
			finish.bitmap = GraphicGet((GameGetGameType() == GameType1vs1) ? GraphicWinX : GraphicWinX);
			break;
	
		case GameStateGameWinO:
			finish.bitmap = GraphicGet((GameGetGameType() == GameType1vs1) ? GraphicWinO : GraphicWinO);
			break;

		case GameStateGameTie:
			finish.bitmap = GraphicGet(GraphicTie);
			break;

		default:
			return;
	}
	
	ViewDraw(&finish);
}