if(NOT DEFINED TOOLCHAIN_CORTEXM3_CONFIGURED)
    message(FATAL_ERROR "The CortexM3 toolchain must be configured before the standard library")
endif()

set(ST_LIB_ROOT /usr/local/lib/stellaris-stdlib CACHE PATH "path to the stellaris standard library")

set(ST_LIB_INCLUDE_DIRS
    ${ST_LIB_ROOT}
    ${ST_LIB_ROOT}/inc
    ${ST_LIB_ROOT}/driverlib
    ${ST_LIB_ROOT}/grlib
)

if(NOT DEFINED BOOT_LOADER)
    set(BOOT_LOADER ${ST_LIB_ROOT}/bootloaders/${BOARD_NAME}.c)
endif()

set(ST_INCLUDE_FILES
    ${ST_LIB_ROOT}/inc/hw_types.h
)

set(ST_SOURCE_FILES
    ${BOOT_LOADER}
)

set(ST_LIBS
    ${ST_LIB_ROOT}/bin/libdriver-cm3.a
    ${ST_LIB_ROOT}/bin/libgr-cm3.a
)

if(NOT DEFINED DEVICE_LINKER_SCRIPT)
    set(DEVICE_LINKER_SCRIPT ${ST_LIB_ROOT}/linker_scripts/${BOARD_NAME}.ld)
endif()

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T${DEVICE_LINKER_SCRIPT}")

set(STELLARIS_STANDARD_LIBRARY_CONFIGURED true)