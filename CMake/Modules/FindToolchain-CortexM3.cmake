set(CMAKE_SYSTEM_NAME      Generic)
set(CMAKE_SYSTEM_VERSION   1)
set(CMAKE_SYSTEM_PROCESSOR arm-eabi)

set(CMAKE_C_COMPILER       arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER     arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER     arm-none-eabi-as)
set(CMAKE_OBJCOPY     	   arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP     	   arm-none-eabi-objdump)

set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "")
set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "")

set(TOOCHAIN_DIR /usr/local/bin/gcc-arm-none-eabi-4_8-2014q2 CACHE PATH "installation path to the GNU Tools for ARM Embedded Processors toolchain")
set(TOOCHAIN_LIB_DIR ${TOOCHAIN_DIR}/lib)

set(DEFAULT_COMPILER_FLAGS  "--specs=nosys.specs")
set(DEFAULT_CPU_FLAGS       "-mthumb -mcpu=cortex-m3")
set(DEFAULT_CFLAGS          "-fno-builtin -Wall -fdata-sections -ffunction-sections")

set(CMAKE_C_FLAGS "${DEFAULT_COMPILER_FLAGS} ${DEFAULT_CPU_FLAGS} ${DEFAULT_CFLAGS} -std=gnu99" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS "${DEFAULT_COMPILER_FLAGS} ${DEFAULT_CPU_FLAGS} ${DEFAULT_CFLAGS}" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_ASM_FLAGS "${DEFAULT_COMPILER_FLAGS} ${DEFAULT_CPU_FLAGS}" CACHE INTERNAL "asm compiler flags")

set(CMAKE_EXE_LINKER_FLAGS "-nostartfiles -Wl,--gc-sections ${DEFAULT_CPU_FLAGS}" CACHE INTERNAL "exe link flags")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(TOOLCHAIN_CORTEXM3_CONFIGURED true)