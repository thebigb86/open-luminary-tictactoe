if(NOT DEFINED EXECUTABLE_CONFIGURED)
    message(FATAL_ERROR "The '${PROJECT_NAME}.bin' target requires that the '${PROJECT_NAME}.elf' target is loaded")
endif()

add_custom_target(
    ${PROJECT_NAME}.bin
    DEPENDS ${PROJECT_NAME}.elf
    COMMAND ${CMAKE_OBJCOPY} -Obinary ${CMAKE_OUTPUT_DIR}/${PROJECT_NAME}.elf ${CMAKE_OUTPUT_DIR}/${PROJECT_NAME}.bin
)

set(BIN_CONFIGURED true)