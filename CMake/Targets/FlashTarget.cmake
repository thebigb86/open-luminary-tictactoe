if(NOT DEFINED BIN_CONFIGURED)
    message(FATAL_ERROR "The 'flash' target requires that the '${PROJECT_NAME}.bin' target is loaded")
endif()

add_custom_target(
    flash
    DEPENDS ${PROJECT_NAME}.bin
    COMMAND openocd -f board/${BOARD_FLASH_CONFIG} -c "program ${CMAKE_OUTPUT_DIR}/${PROJECT_NAME}.bin verify reset"
)

set(FLASH_CONFIGURED true)