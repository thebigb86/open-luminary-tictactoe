string(TOUPPER PART_${BOARD_NAME} PART_NAME)
add_definitions(-D${PART_NAME})

if(DEFINED STELLARIS_STANDARD_LIBRARY_CONFIGURED)

    foreach(INCLUDE_FILE ${ST_INCLUDE_FILES})
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -include ${INCLUDE_FILE}")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -include ${INCLUDE_FILE}")
        set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -include ${INCLUDE_FILE}")
    endforeach(INCLUDE_FILE)

    include_directories(
        ${ST_LIB_INCLUDE_DIRS}
        ${INCLUDE_DIRS}
    )

    add_executable(${PROJECT_NAME}.elf
        ${ST_SOURCE_FILES}
        ${SOURCE_FILES}
    )

    target_link_libraries(${PROJECT_NAME}.elf
        ${ST_LIBS}
        ${LIBS}
    )

else()

    message(WARNING "The Stellaris standard peripheral library was not loaded; compiling bare project.")

    include_directories(
        ${INCLUDE_DIRS}
    )

    add_executable(${PROJECT_NAME}.elf
        ${SOURCE_FILES}
    )

endif()

set(EXECUTABLE_CONFIGURED true)